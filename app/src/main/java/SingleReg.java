package com.example.android.badmintonscoreboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.android.badmintonscoreboard.R;


public class SingleReg extends AppCompatActivity {

    EditText playerAName;
    EditText playerBName;

    Button btnSingleStart;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_reg);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        playerAName = (EditText) findViewById(R.id.nameA);
        playerBName = (EditText) findViewById(R.id.nameB);

    }


    public void doubleRegister(View view) {
        Intent doubleRegisterForm = new Intent(this, DoubleReg.class);

        startActivity(doubleRegisterForm);
    }

    public void startSingle(View view) {
        Intent startSingleGame = new Intent(this, SingleScore.class);
        startSingleGame.putExtra("playerA", playerAName.getText().toString());
        startSingleGame.putExtra("playerB", playerBName.getText().toString());
        startActivity(startSingleGame);
    }
}



